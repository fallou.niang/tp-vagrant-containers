# Vagrant-Containers

#AUTEURS : SAID AHMED Nairate, NIANG Fallou

#Utilisation du code 

Avant d'executer le code assurer vous d'etre dans le bon repertoire : il s'agit ici du repertoire dockerlabglances.

Pour exécuter le code, veuillez suivre les étapes suivantes :

1-vous devez d'abord démarrer vagrant ainsi s'y connecter en ssh avec respectivement ces deux commandes :
Vagrant up 
Vagrant ssh

2-Aller sur le repertoire "seance4" : 
cd dockerlabglances/seance4

3- Assurez vous que tous les fichiers y sont: 
glaces.conf
mosquitto.conf 
docker-compose.yml


3- Si tous ces fichiers s'y trouvent, vous pouvez lancer la commande permettant de démarrer le broker et tout ce qui est demandé: 

- Pour démarrer :
docker-compose up

- Pour l'arreter : docker-compose down 
ou docker-compose down -v

Pour plus d'informations sur cette commande, vous pouvez taper :

docker-compose -h


#URLs :

#mqtt

http://localhost:8000/

#nodered : 
http://localhost:1880/

#nodered_dashboard
http://localhost:1880/ui/#!/0?socketid=J_TTuDhg8YGPlGBQAAAA

